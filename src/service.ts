import axios from "axios";
import type { Collection } from "./entities";



export async function fetchAllCollections() {
    const response = await axios.get<Collection[]>('http://localhost:8080/api/collection');
    return response.data;
}

export async function fetchOneCollection(id:any) {
    const response = await axios.get<Collection>('http://localhost:8080/api/collection/'+id);
    return response.data;
}


export async function postCollection(collection:Collection){
    const response = await axios.post<Collection>('http://localhost:8080/api/collection/', collection);
    return response.data;
}


export async function deleteCollection(id:any) {
    await axios.delete<void>('http://localhost:8080/api/collection/'+id);
}


export async function updateDog(collection:Collection) {
    const response = await axios.put<Collection>('http://localhost:8080/api/collection/'+ collection.id, collection);
    return response.data;
}
