export interface Collection{
    id?: number;
    name: string;
    image: string;
    description: string;
}

export interface Outfit{
    id?: number;
    name: string;
    image: string;
    description: string;
    collection_id: number;
}

export interface User{
    id?: number;
    email: string;
    password: string;
    role: string;
}