import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue';
import CollectionView from '@/views/CollectionView.vue';
import SingleCollectionView from '@/views/SingleCollectionView.vue';


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: HomeView
    },
    {
      path: '/collections',
      component: CollectionView
    },
    {
      path: '/collection/:id',
      component: SingleCollectionView
    }

  
  ]
})

export default router
